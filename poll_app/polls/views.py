from django.shortcuts import get_object_or_404, render
from django.template import loader

from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.urls import reverse
from .models import question, choice
from django.views import generic
from django.utils import timezone

class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_questions'

    def get_queryset(self):
        return question.objects.filter(pub_date__lte=timezone.now()).order_by('-pub_date')

class ResultView(generic.DetailView):
    template_name = 'polls/results.html'
    model = question

class DetailsView(generic.DetailView):
    template_name = 'polls/details.html'
    model = question

def vote(request, q_id):
    ques = get_object_or_404(question, pk = q_id)

    try:
        selected = ques.choice_set.get(pk = request.POST['choice'])

    except (KeyError, choice.DoesNotExist):
        return render(request, 'polls/details.html', {'question' : ques, 'Error' : 'Invalid choice',})

    else:
        selected.votes += 1
        selected.save()
        return HttpResponseRedirect(reverse('polls:result', args=(q_id,)))