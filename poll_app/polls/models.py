from django.db import models
from django.utils import timezone
import datetime

class question(models.Model):
    question_text = models.CharField(max_length = 200)
    pub_date = models.DateTimeField('publication_date')

    def __str__(self):
        return "question text : " + self.question_text + "\npublication date : " + str(self.pub_date)

    def published_recent(self):
        return (timezone.now() - datetime.timedelta(days = 1)) <= self.pub_date <= timezone.now()

class choice(models.Model):
    question = models.ForeignKey(question, on_delete = models.CASCADE)
    choice_text = models.CharField(max_length = 200)
    votes = models.IntegerField(default = 0)

    def __str__(self):
        return "choice text : " + self.choice_text + "\nvotes : " + str(self.votes)