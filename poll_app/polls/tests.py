from django.test import TestCase
import datetime
from django.utils import timezone
from .models import question
from django.urls import reverse

def create_question(question_text, days):
    time = timezone.now() + datetime.timedelta(days=days)
    return question.objects.create(question_text=question_text, pub_date=time)

class question_IndexView_tests(TestCase):
    def test_no_questions(self):
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No polls are available.")
        self.assertQuerysetEqual(response.context['latest_questions'], [])

    def test_past_question(self):
        create_question(question_text="Past question.", days=-30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(response.context['latest_questions'], ['<Question: Past question.>'])

    def test_future_question(self):
        create_question(question_text="Future question.", days=30)
        response = self.client.get(reverse('polls:index'))
        self.assertContains(response, "No polls are available.")
        self.assertQuerysetEqual(response.context['latest_questions'], [])

    def test_future_question_and_past_question(self):
        create_question(question_text="Past question.", days=-30)
        create_question(question_text="Future question.", days=30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_questions'], ['<Question: Past question.>'])

    def test_two_past_questions(self):
        create_question(question_text="Past question 1.", days=-30)
        create_question(question_text="Past question 2.", days=-5)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(response.context['latest_questions'], ['<Question: Past question 2.>', '<Question: Past question 1.>'])

class question_model_test(TestCase):
    def test_published_recent_with_future(self):
        time = timezone.now() + datetime.timedelta(days = 30)
        future_ques = question(pub_date = time)
        self.assertEqual(future_ques.published_recent(), False)

    def test_published_recent_old_question(self):
        time = timezone.now() - datetime.timedelta(days=1, seconds=1)
        old_question = question(pub_date=time)
        self.assertIs(old_question.published_recent(), False)

    def test_published_recent_recent_question(self):
        time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        recent_question = question(pub_date=time)
        self.assertIs(recent_question.published_recent(), True)